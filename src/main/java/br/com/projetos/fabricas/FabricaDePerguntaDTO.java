package br.com.projetos.fabricas;

import java.util.ArrayList;
import java.util.List;

import br.com.projetos.dto.PerguntaDTO;
import br.com.projetos.dto.RespostaDTO;

/**
 * 
 * @author jefferson.souza
 *
 */

public interface FabricaDePerguntaDTO {

	/**
	 * Fabrica de pergunta
	 * 
	 * @param valorPergunta perguntaString
	 * @param respostasDTO lista de respostasDTO
	 * @return perguntaDTO
	 */
	default PerguntaDTO construirPerguntaDTO(String valorPergunta, List<RespostaDTO> respostasDTO) {
		return new PerguntaDTO().setValor(valorPergunta).setRespostasDTO(respostasDTO);
	}

	/**
	 * Fabrica de respostasDTO
	 * 
	 * @param valoresDeResposta lista de respostas String
	 * @return lista de respostasDTO
	 */
	default List<RespostaDTO> construirRespostasDTO(List<String> valoresDeResposta) {

		List<RespostaDTO> respostasDTO = new ArrayList<>();

		valoresDeResposta.forEach(valor -> respostasDTO.add(construirRespostaDTO(valor)));

		return respostasDTO;

	}

	/**
	 * Fabrica de Resposta
	 * 
	 * @return respostaDTO
	 */
	default RespostaDTO construirRespostaDTO(String valor) {
		return new RespostaDTO().setValor(valor);
	}

}
