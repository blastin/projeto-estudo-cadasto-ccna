package br.com.projetos.fabricas;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.projetos.dto.PerguntaDTO;
import br.com.projetos.modelos.Pergunta;

/**
 * 
 * @author jefferson.lisboa
 *
 */
@Component
public class FabricaDePergunta {

	private final ModelMapper modelMapper;

	@Autowired
	public FabricaDePergunta(ModelMapper modelMapper) {
		this.modelMapper = modelMapper;
	}

	/*
	 * 
	 */
	public Pergunta construirPergunta(PerguntaDTO perguntaDTO) {
		return modelMapper.map(perguntaDTO, Pergunta.class);
	}
}