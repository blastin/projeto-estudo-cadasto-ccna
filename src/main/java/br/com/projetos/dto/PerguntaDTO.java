package br.com.projetos.dto;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author jefferson.lisboa
 *
 */
public class PerguntaDTO {

	private String valor;

	@JsonProperty(value = "respostas")
	private List<RespostaDTO> respostasDTO;

	/**
	 * @return the valor
	 */
	public String getValor() {
		return valor;
	}

	/**
	 * @param valor the valor to set
	 */
	public PerguntaDTO setValor(String valor) {
		this.valor = valor;
		return this;
	}

	/**
	 * @return the respostasDTO
	 */
	public List<RespostaDTO> getRespostasDTO() {
		return Optional.ofNullable(respostasDTO).orElse(Collections.emptyList());
	}

	/**
	 * @param respostasDTO the respostasDTO to set
	 */
	public PerguntaDTO setRespostasDTO(List<RespostaDTO> respostasDTO) {
		this.respostasDTO = respostasDTO;
		return this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((respostasDTO == null) ? 0 : respostasDTO.hashCode());
		result = prime * result + ((valor == null) ? 0 : valor.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PerguntaDTO other = (PerguntaDTO) obj;
		if (respostasDTO == null) {
			if (other.respostasDTO != null)
				return false;
		} else if (!getRespostasDTO().equals(other.respostasDTO)) {
			return false;
		}
		if (getValor() == null) {
			return other.valor == null;
		} else return getValor().equals(other.valor);
	}

}
