package br.com.projetos.enumeracoes;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Optional;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;

/**
 * Modelo de dados => RELACIONAL : {"VALOR VOLATIL COMO MENSAGEM", "VALOR
 * IMUTAVEL"}
 * 
 * @author jefferson.lisboa
 *
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum ProtocoloEnum implements Serializable {

	PERGUNTA_DEVE_CONTER_PELO_MENOS_UMA_RESPOSTA(64000, "PERGUNTA DEVE CONTER PELO MENOS UMA RESPOSTA"),
	PERGUNTA_NAO_BRANCA_OU_NULA(65000, "PERGUNTA NÃO DEVE SER EM BRANCO OU NULA"),
	RESPOSTA_NAO_BRANCA_OU_NULA(66000, "RESPOSTA NÃO DEVE SER EM BRANCO OU NULA"),
	VIOLACAO_DE_VALOR(67000, "VIOLACAO DE VALOR NAO IDENTIFICADO");

	private static final String MENSAGEM_CONSTANT = "mensagem";
	private static final String STATUS_CONSTANT = "status";
	
	private final int status;
	private final String mensagem;

	/**
	 * 
	 * @param mensagem mensagem do protocolo
	 * @param status status em tipagem inteiro
	 */
    ProtocoloEnum(int status, String mensagem) {
		this.mensagem = mensagem;
		this.status = status;
	}

	/**
	 * @return the mensagem
	 */
	@JsonProperty
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * @return the status
	 */
	@JsonProperty
	private int getStatus() {
		return status;
	}

	@JsonCreator
	public static ProtocoloEnum fromNode(JsonNode node) {

		if (!node.has(STATUS_CONSTANT) || !node.has(MENSAGEM_CONSTANT))
			return null;

		int status = node.get(STATUS_CONSTANT).asInt();

		Optional<ProtocoloEnum> protocoloPossivel = Arrays.stream(ProtocoloEnum.values())
				.filter(protocolo -> protocolo.getStatus() == status).findFirst();

		return protocoloPossivel.orElse(null);
	}

}
