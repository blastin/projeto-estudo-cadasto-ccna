package br.com.projetos.excessoes;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.validation.ConstraintViolation;
import javax.validation.metadata.ConstraintDescriptor;

import com.fasterxml.jackson.annotation.JsonFormat;

import br.com.projetos.anotacoes.Anotacao;
import br.com.projetos.enumeracoes.ProtocoloEnum;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public class Violacoes {

	private List<ProtocoloEnum> protocolos;

	/**
	 * 
	 */
	Violacoes() {
		protocolos = new ArrayList<>();
	}

	/**
	 * 
	 * @return lista de procotolos
	 */
	public List<ProtocoloEnum> getProtocolos() {
		return protocolos;
	}

	/**
	 * 
	 * @param protocolos lista de procotolos
	 */
	public void setProtocolos(List<ProtocoloEnum> protocolos) {
		this.protocolos = protocolos;
	}

	/**
	 * 
	 * @param violacao violacao
	 */
	void inserirViolacao(ConstraintViolation<?> violacao) {
		Object protocolo = obterProtocoloDeConstraintDescriptor(violacao.getConstraintDescriptor());
		protocolos.add((ProtocoloEnum) protocolo);
	}

	/**
	 * Manipulando Violação
	 * 
	 * @param constraintDescriptor descriptor de violação
	 * @return Protocolo
	 */
	private Object obterProtocoloDeConstraintDescriptor(ConstraintDescriptor<?> constraintDescriptor) {
		return obterProtocoloDeAtributos(constraintDescriptor.getAttributes());
	}

	/**
	 * 
	 * @param atributos mapa de atributos de anotações
	 * @return protocolo
	 */
	private Object obterProtocoloDeAtributos(Map<String, Object> atributos) {
		return atributos.get(Anotacao.PROTOCOLO);
	}

}
