package br.com.projetos.controles;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.projetos.dto.PerguntaDTO;
import br.com.projetos.modelos.Pergunta;
import br.com.projetos.servicos.PerguntasServico;

/**
 * @author jefferson.lisboa
 *
 */
@RestController
public class PerguntasControle {

	private final PerguntasServico servico;

	@Autowired
	public PerguntasControle(PerguntasServico servico) {
		this.servico = servico;
	}

	@GetMapping(path = "/perguntas/todas")
	public List<Pergunta> obterPerguntas() {
		return servico.obterPerguntas();
	}

	@PostMapping(path = "/perguntas/cadastro")
	public ResponseEntity<PerguntaDTO> cadastrarPergunta(@RequestBody PerguntaDTO perguntaDTO) {
		return servico.cadastrarPergunta(perguntaDTO);
	}

}
