package br.com.projetos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CadastroCcnaApplication {

	public static void main(String[] args) {
		SpringApplication.run(CadastroCcnaApplication.class, args);
	}

}
