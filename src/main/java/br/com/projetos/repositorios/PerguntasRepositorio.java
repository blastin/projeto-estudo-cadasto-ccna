package br.com.projetos.repositorios;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.projetos.modelos.Pergunta;

/**
 * 
 * @author jefferson.lisboa
 *
 */
public interface PerguntasRepositorio extends JpaRepository<Pergunta, Long> {
}
