package br.com.projetos.anotacoes;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.ReportAsSingleViolation;
import javax.validation.constraints.NotEmpty;

import br.com.projetos.enumeracoes.ProtocoloEnum;

@NotEmpty
@ReportAsSingleViolation
@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER })
@Retention(RUNTIME)
@Constraint(validatedBy = {})
public @interface NaoVazio {

	String message() default "{org.hibernate.validator.constraints.NotEmpty.message}";

	ProtocoloEnum protocolo() default ProtocoloEnum.VIOLACAO_DE_VALOR;

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
