package br.com.projetos.modelos;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.validation.annotation.Validated;

import br.com.projetos.anotacoes.NaoBranco;
import br.com.projetos.enumeracoes.ProtocoloEnum;

/**
 * 
 * @author jefferson.lisboa
 *
 */
@Entity
@Table(name = "Respostas")
public class Resposta {

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	private long id;

	@NaoBranco(protocolo = ProtocoloEnum.RESPOSTA_NAO_BRANCA_OU_NULA)
	private String valor;

	@ManyToOne(targetEntity = Pergunta.class)
	private Pergunta pergunta;

	/**
	 * @return the valor
	 */
	public String getValor() {
		return valor;
	}

	/**
	 * 
	 * @param valor respostaString
	 * @return resposta Instance
	 */
	@Validated
	public Resposta setValor(String valor) {
		this.valor = valor;
		return this;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((valor == null) ? 0 : valor.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Resposta other = (Resposta) obj;
		if (getId() != other.id)
			return false;
		if (getValor() == null) {
			return other.valor == null;
		} else return valor.equals(other.valor);
	}

}
