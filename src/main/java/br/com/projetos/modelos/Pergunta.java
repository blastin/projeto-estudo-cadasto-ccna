package br.com.projetos.modelos;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.validation.annotation.Validated;

import br.com.projetos.anotacoes.NaoBranco;
import br.com.projetos.anotacoes.NaoVazio;
import br.com.projetos.enumeracoes.ProtocoloEnum;

/**
 * 
 * @author jefferson.lisboa
 *
 */
@Entity
@Table(name = "Perguntas")
public class Pergunta {

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	private long id;

	@NaoBranco(protocolo = ProtocoloEnum.PERGUNTA_NAO_BRANCA_OU_NULA)
	private String valor;

	@NaoVazio(protocolo = ProtocoloEnum.PERGUNTA_DEVE_CONTER_PELO_MENOS_UMA_RESPOSTA)
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Resposta> respostas;

	/**
	 * @return the valor
	 */
	public String getValor() {
		return valor;
	}

	/**
	 * @param valor the valor to set
	 */
	@Validated
	public Pergunta setValor(String valor) {
		this.valor = valor;
		return this;
	}

	/**
	 * @return the respostas
	 */
	public List<Resposta> getRespostas() {
		return respostas;
	}

	/**
	 * @param respostas the respostas to set
	 */
	@Validated
	public Pergunta setRespostas(List<Resposta> respostas) {
		this.respostas = respostas;
		return this;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((respostas == null) ? 0 : respostas.hashCode());
		result = prime * result + ((valor == null) ? 0 : valor.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pergunta other = (Pergunta) obj;
		if (getId() != other.id)
			return false;
		if (respostas == null) {
			if (other.respostas != null)
				return false;
		} else if (!getRespostas().equals(other.respostas)) {
			return false;
		}
		if (valor == null) {
			return other.valor == null;
		} else return getValor().equals(other.valor);
	}

}
