package br.com.projetos.servicos;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import br.com.projetos.dto.PerguntaDTO;
import br.com.projetos.fabricas.FabricaDePergunta;
import br.com.projetos.modelos.Pergunta;
import br.com.projetos.repositorios.PerguntasRepositorio;

/**
 * @author jefferson.lisboa
 */
@Service
public class PerguntasServico {

    private final PerguntasRepositorio repositorio;

    private final FabricaDePergunta fabricaDePergunta;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    public PerguntasServico(PerguntasRepositorio repositorio, FabricaDePergunta fabricaDePergunta) {
        this.repositorio = repositorio;
        this.fabricaDePergunta = fabricaDePergunta;
    }

    /**
     * Obter lista de todas as perguntas com suas respectivas respostas
     *
     * @return lista de perguntas
     */
    public List<Pergunta> obterPerguntas() {
        return repositorio.findAll();
    }

    /**
     * Cadastrar pergunta e suas respectivas respostas.
     *
     * @param perguntaDTO Pergunta
     * @return response
     */
    public ResponseEntity<PerguntaDTO> cadastrarPergunta(PerguntaDTO perguntaDTO) {
        Pergunta perguntaParse = fabricaDePergunta.construirPergunta(perguntaDTO);
        repositorio.save(perguntaParse);
        return ResponseEntity.ok().body(perguntaDTO);
    }

}
