package br.com.projetos;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import br.com.projetos.dto.PerguntaDTO;
import br.com.projetos.dto.RespostaDTO;
import br.com.projetos.fabricas.FabricaDePergunta;
import br.com.projetos.modelos.Pergunta;
import br.com.projetos.modelos.Resposta;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class, classes = TestAppConfig.class)
public class PerguntaTest extends FabricaDePerguntasAbstrato {

	@Autowired
	private FabricaDePergunta fabrica;

	@Test
	public void duasPerguntasDTOIguaisTest() {

		/*
		  Construção
		 */
		PerguntaDTO A = construtorPerguntaDTO();
		PerguntaDTO B = construtorPerguntaDTO();

		Assert.assertEquals(A, B);
		Assert.assertEquals(A, A);
		Assert.assertEquals(B, B);
		Assert.assertEquals(A.hashCode(), B.hashCode());
	}

	@Test
	public void duasPerguntasIguaisTest() {

		/*
		  Construção
		 */
		PerguntaDTO ADTO = construtorPerguntaDTO();
		PerguntaDTO BDTO = construtorPerguntaDTO();

		Pergunta A = fabrica.construirPergunta(ADTO);
		Pergunta B = fabrica.construirPergunta(BDTO);

		Assert.assertEquals(A, B);
		Assert.assertEquals(A, A);
		Assert.assertEquals(B, B);
		Assert.assertEquals(A.hashCode(), B.hashCode());
	}

	@Test
	public void respostasDTOIguaisTest() {

		/*
		  Construção
		 */
		RespostaDTO A = new RespostaDTO().setValor("A");
		RespostaDTO B = new RespostaDTO().setValor("A");

		Assert.assertEquals(A, B);
		Assert.assertEquals(A, A);
		Assert.assertEquals(B, B);

	}

	@Test
	public void respostasIguaisTest() {

		/*
		  Construção
		 */
		RespostaDTO ADTO = new RespostaDTO().setValor("A");
		RespostaDTO BDTO = new RespostaDTO().setValor("A");

		Resposta A = new Resposta().setValor(ADTO.getValor());
		Resposta B = new Resposta().setValor(BDTO.getValor());

		Assert.assertEquals(A, B);
		Assert.assertEquals(A, A);
		Assert.assertEquals(B, B);

	}

	@Test
	public void duasPerguntasDTODiferentesTest() {

		/*
		  Construção
		 */
		PerguntaDTO A = construtorPerguntaDTO();
		PerguntaDTO B = construirPerguntaDTO("B", construirRespostasDTO(Arrays.asList("B", "BBB", "BBB")));

		Assert.assertNotEquals(A, B);
		Assert.assertNotEquals(A, null);
		Assert.assertNotEquals(B, null);
		Assert.assertNotEquals(A, new Object());
		Assert.assertNotEquals(new PerguntaDTO(), B);
		Assert.assertNotEquals(new PerguntaDTO().setRespostasDTO(construtorRespostasDTO()), A);
		Assert.assertNotEquals(new PerguntaDTO().setRespostasDTO(construtorRespostasDTO()).setValor("C"), A);

	}

	@Test
	public void duasPerguntasDiferentesTest() {

		/*
		  Construção
		 */
		PerguntaDTO ADTO = construtorPerguntaDTO();
		PerguntaDTO BDTO = construirPerguntaDTO("B", construirRespostasDTO(Arrays.asList("B", "BBB", "BBB")));

		Pergunta A = fabrica.construirPergunta(ADTO);
		Pergunta B = fabrica.construirPergunta(BDTO);

		Assert.assertNotEquals(A, B);
		Assert.assertNotEquals(A, null);
		Assert.assertNotEquals(B, null);
		Assert.assertNotEquals(new Pergunta(), B);

		Assert.assertNotEquals(A, new Object());

		Pergunta C = fabrica.construirPergunta(new PerguntaDTO().setRespostasDTO(construtorRespostasDTO()));
		Assert.assertNotEquals(C, A);

		Pergunta D = fabrica
				.construirPergunta(new PerguntaDTO().setRespostasDTO(construtorRespostasDTO()).setValor("C"));
		Assert.assertNotEquals(D, A);

	}

	@Test
	public void respostasDTODistintasTest() {

		/*
		  Construção
		 */
		RespostaDTO A = new RespostaDTO().setValor("A");
		RespostaDTO B = new RespostaDTO().setValor("B");

		Assert.assertNotEquals(A, B);
		Assert.assertNotEquals(A, null);
		Assert.assertNotEquals(A, new Object());

		RespostaDTO C = new RespostaDTO();

		Assert.assertNotEquals(C, B);
	}

	@Test
	public void respostasDistintasTest() {

		/*
		  Construção
		 */
		RespostaDTO ADTO = new RespostaDTO().setValor("A");
		RespostaDTO BDTO = new RespostaDTO().setValor("B");

		Resposta A = new Resposta().setValor(ADTO.getValor());
		Resposta B = new Resposta().setValor(BDTO.getValor());

		Assert.assertNotEquals(A, B);
		Assert.assertNotEquals(A, null);
		Assert.assertNotEquals(A, null);
		Assert.assertNotEquals(A, new Object());

		Resposta C = new Resposta();

		Assert.assertNotEquals(C, B);

	}

}
