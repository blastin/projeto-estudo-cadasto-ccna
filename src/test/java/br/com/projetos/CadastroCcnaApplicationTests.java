package br.com.projetos;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.projetos.dto.PerguntaDTO;
import br.com.projetos.dto.RespostaDTO;
import br.com.projetos.enumeracoes.ProtocoloEnum;
import br.com.projetos.excessoes.Violacoes;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CadastroCcnaApplicationTests extends FabricaDePerguntasAbstrato {

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public void cadastroEValidacaoDePerguntaRespostasTest() {

		/*
		  recebe uma massa de dados do tipo pergunta
		 */
		PerguntaDTO perguntaEnviadaParaCadastro = construtorPerguntaDTO();

		/*
		  Realiza uma requisição do tipo rest, utilizando o verbo post.

		  POST: Necessita de um dados de requisição (nesse caso é o objeto Pergunta)
		  RESPOSTA: STATUS, PERGUNTA SALVA
		 */
		ResponseEntity<PerguntaDTO> perguntaRespostasCadastro = restTemplate.postForEntity(construtorURLCadastro(),
				perguntaEnviadaParaCadastro, PerguntaDTO.class);

		/*
		  Valida se o status recebido é exatamente 200, configurado no serviço
		 */
		Assert.assertEquals("Status precisa ser Ok", HttpStatus.OK, perguntaRespostasCadastro.getStatusCode());

		/*
		  Compara a resposta com a pergunta cadastrada
		 */
		Assert.assertEquals("espero a pergunta cadastrada", perguntaEnviadaParaCadastro,
				perguntaRespostasCadastro.getBody());

		/*
		  Realiza uma requisição do tipo rest, utilizando o verbo get.

		  GET: STATUS, E PERGUNTAS SALVAS
		 */
		ResponseEntity<PerguntaDTO[]> perguntasRespostaObter = restTemplate.getForEntity(construtorURLObterPerguntas(),
				PerguntaDTO[].class);

		/*
		  Valida se o status recebido é exatamente 200, configurado no serviço
		 */
		Assert.assertEquals("Status precisa ser Ok", HttpStatus.OK, perguntasRespostaObter.getStatusCode());

		/*
		  Verificar se existe exatamente 1 pergunta recebida.
		 */
		Assert.assertEquals("Deve ter apenas uma pergunta cadastrada", 1L, Objects.requireNonNull(perguntasRespostaObter.getBody()).length);

		/*
		  Compara a resposta com a pergunta cadastrada
		 */
		Assert.assertEquals("espero a pergunta cadastrada", perguntaEnviadaParaCadastro,
				perguntasRespostaObter.getBody()[0]);
	}

	@Test
	public void cadastrarPerguntaComRespostEmBrancoTest() {

		/*
		  Construir pergunta com valor em branco
		 */
		PerguntaDTO perguntaEnviadaParaCadastro = new PerguntaDTO();
		perguntaEnviadaParaCadastro.setValor("Hello world")
				.setRespostasDTO(Collections.singletonList(new RespostaDTO().setValor("")));

		/*
		  Realiza uma requisição do tipo rest, utilizando o verbo post.

		  POST: Necessita de um dados de requisição (nesse caso é o objeto Pergunta)
		  RESPOSTA: STATUS, PERGUNTA SALVA
		 */
		ResponseEntity<Violacoes> violacoes = restTemplate.postForEntity(construtorURLCadastro(),
				perguntaEnviadaParaCadastro, Violacoes.class);

		/*
		  Valida se o status recebido é exatamente 400, configurado no serviço
		 */
		Assert.assertEquals("Status precisa ser BAD_REQUEST", HttpStatus.BAD_REQUEST, violacoes.getStatusCode());

		/*
		  lista de protocolos
		 */
		List<ProtocoloEnum> protocolos = Objects.requireNonNull(violacoes.getBody()).getProtocolos();

		/*
		  Verificar se existe exatamente 1 protocolo recebido
		 */
		Assert.assertEquals("Deve ter apenas um protocolo retornado", 1L, protocolos.size());

		/*
		  Verificar se protocolo recebido é exatamente o protocolo esperado
		 */
		Assert.assertEquals(ProtocoloEnum.RESPOSTA_NAO_BRANCA_OU_NULA, protocolos.get(0));

	}

	@Test
	public void cadastrarPerguntaComRespostasNulaTest() {

		/*
		  Construir pergunta com valor em branco
		 */
		PerguntaDTO perguntaEnviadaParaCadastro = new PerguntaDTO();
		perguntaEnviadaParaCadastro.setValor("Hello world").setRespostasDTO(null);

		/*
		  Realiza uma requisição do tipo rest, utilizando o verbo post.

		  POST: Necessita de um dados de requisição (nesse caso é o objeto Pergunta)
		  RESPOSTA: STATUS, PERGUNTA SALVA
		 */
		ResponseEntity<Violacoes> violacoes = restTemplate.postForEntity(construtorURLCadastro(),
				perguntaEnviadaParaCadastro, Violacoes.class);

		/*
		  Valida se o status recebido é exatamente 400, configurado no serviço
		 */
		Assert.assertEquals("Status precisa ser BAD_REQUEST", HttpStatus.BAD_REQUEST, violacoes.getStatusCode());

		/*
		  lista de protocolos
		 */
		List<ProtocoloEnum> protocolos = Objects.requireNonNull(violacoes.getBody()).getProtocolos();

		/*
		  Verificar se existe exatamente 1 protocolo recebido
		 */
		Assert.assertEquals("Deve ter apenas um protocolo retornado", 1L, protocolos.size());

		/*
		  Verificar se protocolo recebido é exatamente o protocolo esperado
		 */
		Assert.assertEquals(ProtocoloEnum.PERGUNTA_DEVE_CONTER_PELO_MENOS_UMA_RESPOSTA, protocolos.get(0));

	}

	@Test
	public void cadastrarPerguntaEmBrancoTest() {

		/*
		  Construir pergunta com valor em branco
		 */
		PerguntaDTO perguntaEnviadaParaCadastro = new PerguntaDTO();
		perguntaEnviadaParaCadastro.setValor("").setRespostasDTO(Collections.singletonList(new RespostaDTO().setValor("RESPOSTA")));

		/*
		  Realiza uma requisição do tipo rest, utilizando o verbo post.

		  POST: Necessita de um dados de requisição (nesse caso é o objeto Pergunta)
		  RESPOSTA: STATUS, PERGUNTA SALVA
		 */
		ResponseEntity<Violacoes> violacoes = restTemplate.postForEntity(construtorURLCadastro(),
				perguntaEnviadaParaCadastro, Violacoes.class);

		/*
		  Valida se o status recebido é exatamente 400, configurado no serviço
		 */
		Assert.assertEquals("Status precisa ser BAD_REQUEST", HttpStatus.BAD_REQUEST, violacoes.getStatusCode());

		/*
		  lista de protocolos
		 */
		List<ProtocoloEnum> protocolos = Objects.requireNonNull(violacoes.getBody()).getProtocolos();

		/*
		  Verificar se existe exatamente 1 protocolo recebido
		 */
		Assert.assertEquals("Deve ter apenas um protocolo retornado", 1L, protocolos.size());

		/*
		  Verificar se protocolo recebido é exatamente o protocolo esperado
		 */
		Assert.assertEquals(ProtocoloEnum.PERGUNTA_NAO_BRANCA_OU_NULA, protocolos.get(0));

	}

	@Test
	public void cadastrarPerguntaNulaTest() {

		/*
		  Construir pergunta com valor em branco
		 */
		PerguntaDTO perguntaEnviadaParaCadastro = new PerguntaDTO();
		perguntaEnviadaParaCadastro.setValor(null)
				.setRespostasDTO(Collections.singletonList(new RespostaDTO().setValor("RESPOSTA")));

		/*
		  Realiza uma requisição do tipo rest, utilizando o verbo post.

		  POST: Necessita de um dados de requisição (nesse caso é o objeto Pergunta)
		  RESPOSTA: STATUS, PERGUNTA SALVA
		 */
		ResponseEntity<Violacoes> violacoes = restTemplate.postForEntity(construtorURLCadastro(),
				perguntaEnviadaParaCadastro, Violacoes.class);

		/*
		  Valida se o status recebido é exatamente 400, configurado no serviço
		 */
		Assert.assertEquals("Status precisa ser BAD_REQUEST", HttpStatus.BAD_REQUEST, violacoes.getStatusCode());

		/*
		  lista de protocolos
		 */
		List<ProtocoloEnum> protocolos = Objects.requireNonNull(violacoes.getBody()).getProtocolos();

		/*
		  Verificar se existe exatamente 1 protocolo recebido
		 */
		Assert.assertEquals("Deve ter apenas um protocolo retornado", 1L, protocolos.size());

		/*
		  Verificar se protocolo recebido é exatamente o protocolo esperado
		 */
		Assert.assertEquals(ProtocoloEnum.PERGUNTA_NAO_BRANCA_OU_NULA, protocolos.get(0));

	}

	@Test
	public void cadastrarPerguntaNulaERespostaNulaTest() {

		/*
		  Construir pergunta com valor em branco
		 */
		PerguntaDTO perguntaEnviadaParaCadastro = new PerguntaDTO();
		perguntaEnviadaParaCadastro.setValor(null).setRespostasDTO(Collections.singletonList(new RespostaDTO().setValor(null)));

		/*
		  Realiza uma requisição do tipo rest, utilizando o verbo post.

		  POST: Necessita de um dados de requisição (nesse caso é o objeto Pergunta)
		  RESPOSTA: STATUS, PERGUNTA SALVA
		 */
		ResponseEntity<Violacoes> violacoes = restTemplate.postForEntity(construtorURLCadastro(),
				perguntaEnviadaParaCadastro, Violacoes.class);

		/*
		  Valida se o status recebido é exatamente 400, configurado no serviço
		 */
		Assert.assertEquals("Status precisa ser BAD_REQUEST", HttpStatus.BAD_REQUEST, violacoes.getStatusCode());

		/*
		  lista de protocolos
		 */
		List<ProtocoloEnum> protocolos = Objects.requireNonNull(violacoes.getBody()).getProtocolos();

		/*
		  Verificar se existe exatamente 1 protocolo recebido
		 */
		Assert.assertEquals("Deve ter apenas um protocolo retornado", 1L, protocolos.size());

		/*
		  Verificar se protocolo recebido é exatamente o protocolo esperado
		 */
		Assert.assertEquals(ProtocoloEnum.PERGUNTA_NAO_BRANCA_OU_NULA, protocolos.get(0));
	}

	@Test
	public void cadastrarPerguntaNulaERespostasNulaTest() {

		/*
		  Construir pergunta com valor em branco
		 */
		PerguntaDTO perguntaEnviadaParaCadastro = new PerguntaDTO();

		/*
		  Realiza uma requisição do tipo rest, utilizando o verbo post.

		  POST: Necessita de um dados de requisição (nesse caso é o objeto Pergunta)
		  RESPOSTA: STATUS, PERGUNTA SALVA
		 */
		ResponseEntity<Violacoes> violacoes = restTemplate.postForEntity(construtorURLCadastro(),
				perguntaEnviadaParaCadastro, Violacoes.class);

		/*
		  Valida se o status recebido é exatamente 400, configurado no serviço
		 */
		Assert.assertEquals("Status precisa ser BAD_REQUEST", HttpStatus.BAD_REQUEST, violacoes.getStatusCode());

		/*
		  lista de protocolos
		 */
		List<ProtocoloEnum> protocolosResponse = Objects.requireNonNull(violacoes.getBody()).getProtocolos();

		/*
		  Verificar se existe exatamente 2 protocolo recebido
		 */
		Assert.assertEquals("Deve ter apenas um protocolo retornado", 2L, protocolosResponse.size());

		/*
		  Realizando ordenação em protocolosResponse
		 */
		sortProtocolos(protocolosResponse);

		/*
		  Verificar se a lista de protocolos recebido é exatamente igual a lista de
		  protocolos esperado
		 */
		List<ProtocoloEnum> protocolosEsperado = Arrays.asList(ProtocoloEnum.PERGUNTA_NAO_BRANCA_OU_NULA,
				ProtocoloEnum.PERGUNTA_DEVE_CONTER_PELO_MENOS_UMA_RESPOSTA);

		/*
		  Realizando ordenação em protocolosEsperado
		 */
		sortProtocolos(protocolosEsperado);

		/*
		  Verificando se ambos Arrays ordenados, são iguais
		 */
		Assert.assertArrayEquals(protocolosEsperado.toArray(), protocolosResponse.toArray());

	}

	/**
	 * 
	 * @param protocolosResponse orderna protocolos
	 */
	private void sortProtocolos(List<ProtocoloEnum> protocolosResponse) {
		Collections.sort(protocolosResponse);
	}

	/**
	 * END-POINT : CADASTRAR UMA PERGUNTA
	 * 
	 * @return obtem end-point para cadastro de pergunta
	 */
	private String construtorURLCadastro() {
		return construtorURL() + "/perguntas/cadastro";
	}

	/**
	 * END-POINT : OBTER PERGUNTAS
	 * 
	 * @return obtem end-point de lista de todas perguntas
	 */
	private String construtorURLObterPerguntas() {
		return construtorURL() + "/perguntas/todas";
	}

	/**
	 * Método de construção de URL Padrão
	 * 
	 * @return obtem end-point para dominio
	 */
	private String construtorURL() {
		return "http://localhost:" + port;
	}

}