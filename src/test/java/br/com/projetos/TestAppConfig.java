package br.com.projetos;

import org.springframework.context.annotation.Bean;

import br.com.projetos.config.AppConfig;
import br.com.projetos.fabricas.FabricaDePergunta;

class TestAppConfig extends AppConfig {

	@Bean
	public FabricaDePergunta fabricaDePergunta() {
		return new FabricaDePergunta(super.modelMapper());
	}
}
