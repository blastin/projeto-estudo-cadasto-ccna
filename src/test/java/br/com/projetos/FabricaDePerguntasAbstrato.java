package br.com.projetos;

import java.util.Arrays;
import java.util.List;

import br.com.projetos.dto.PerguntaDTO;
import br.com.projetos.dto.RespostaDTO;
import br.com.projetos.fabricas.FabricaDePerguntaDTO;

/**
 * 
 * @author jefferson.souza
 *
 */
abstract class FabricaDePerguntasAbstrato implements FabricaDePerguntaDTO {

	/**
	 * Fabrica de pergunta
	 * 
	 * @return fabrica de pergunta
	 */
	final PerguntaDTO construtorPerguntaDTO() {
		return construirPerguntaDTO("A company is contemplating whether to use a client/server or a peer-to-peer network",
				construtorRespostasDTO());
	}

	/**
	 * Fabrica de Respostas
	 * 
	 * @return fabrica de respostasDTO
	 */
	List<RespostaDTO> construtorRespostasDTO() {
		return construirRespostasDTO(Arrays.asList("better security", "easy to create", "internet explorer ativo"));
	}

}