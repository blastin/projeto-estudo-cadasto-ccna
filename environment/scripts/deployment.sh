
#!/bin/sh
#
# This file is part of estudo-cadastro-ccna.
#
# estudo-cadastro-ccna is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# estudo-cadastro-ccna is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with estudo-cadastro-ccna.  If not, see <https://www.gnu.org/licenses/>6.
#
# @Autor: Jefferson Lisboa <lisboa.jeff@gmail.com>
#
# RUN: deployment.sh *.yml registry_url username nameproject name-ref-commit
#
#
#VARIAVEIS
FILE="$1"
URL_REGISTRY_DOCKER="$2"
USERNAME="$3"
CI_PROJECT_NAME="$4"
CI_COMMIT_REF_NAME="$5"
#
#
# COMANDOS SED
#
#
#SUBSTITUIR __URL_REGISTRY_DOCKER__
sed -i "s/__URL_REGISTRY_DOCKER__/$URL_REGISTRY_DOCKER/g" "$FILE"
#
#SUBSTITUIR __USERNAME__
sed -i "s/__USERNAME__/$USERNAME/g" "$FILE"
#
#SUBSTITUIR __CI_PROJECT_NAME__
sed -i "s/__CI_PROJECT_NAME__/$CI_PROJECT_NAME/g" "$FILE"
#
#SUBSTITUIR __CI_COMMIT_REF_NAME__
sed -i "s/__CI_COMMIT_REF_NAME__/$CI_COMMIT_REF_NAME/g" "$FILE"
#
