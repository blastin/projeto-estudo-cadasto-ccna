
#!/bin/sh
#
# This file is part of estudo-cadastro-ccna.
#
# estudo-cadastro-ccna is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# estudo-cadastro-ccna is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with estudo-cadastro-ccna.  If not, see <https://www.gnu.org/licenses/>6.
#
# @Autor: Jefferson Lisboa <lisboa.jeff@gmail.com>
#
# RUN: service.sh *.yml nameproject name-ref-commit
#
#
#VARIAVEIS
FILE="$1"
CI_PROJECT_NAME="$2"
CI_COMMIT_REF_NAME="$3"
CLUSTER_SPECIFIC_DNS_ZONE="$4"
#
#
# COMANDOS SED
#
#
#SUBSTITUIR __CI_PROJECT_NAME__
sed -i "s/__CI_PROJECT_NAME__/$CI_PROJECT_NAME/g" "$FILE"
#
#SUBSTITUIR __CI_COMMIT_REF_NAME__
sed -i "s/__CI_COMMIT_REF_NAME__/$CI_COMMIT_REF_NAME/g" "$FILE"
#
#SUBSTITUIR __CLUSTER_SPECIFIC_DNS_ZONE__
sed -i "s/__CLUSTER_SPECIFIC_DNS_ZONE__/$CLUSTER_SPECIFIC_DNS_ZONE/g" "$FILE"
#