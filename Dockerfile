#Base Imagem
FROM lisboajeff/alpine-openjdk8-jre:latest

#COPIAR JAR PARA CONTAINER
COPY target/*.jar deploy.jar

#EXPOR PORTA 8080
EXPOSE 8080

#COMANDO DE INICIALIZAÇÃO
CMD [ "/usr/lib/jvm/java-1.8-openjdk/bin/java", "-jar", "deploy.jar" ]
